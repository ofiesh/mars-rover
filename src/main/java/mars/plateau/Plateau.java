package mars.plateau;

public class Plateau {
	private final Integer width;
	private final Integer length;

	public Plateau(Integer width, Integer length) {
		this.width = width;
		this.length = length;
	}

	public Integer getWidth() {
		return width;
	}

	public Integer getLength() {
		return length;
	}
}

package mars.plateau;

public enum CardinalDirection {
	N(0, 0, 1), E(1, 1, 0), S(2, 0, -1), W(3, -1, 0);

	private final int turn90;
	private final int turn270;
	private final int xChange;
	private final int yChange;
	CardinalDirection(int ordinal, int xChange, int yChange) {
		this.turn90 = (ordinal + 1) % 4;
		this.turn270 = (ordinal + 3) % 4;
		this.xChange = xChange;
		this.yChange = yChange;
	}

	public CardinalDirection rotate(TurnDirection direction) {
		return values()[TurnDirection.R.equals(direction) ? turn90 : turn270];
	}

	public Position move(Position position) {
		return new Position(position.getX() + xChange, position.getY() + yChange);
	}
}

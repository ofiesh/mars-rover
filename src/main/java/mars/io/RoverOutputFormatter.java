package mars.io;

import mars.rover.Rover;

public class RoverOutputFormatter {
	public String format(Rover rover) {
		return String.format("%s %s %s",
				rover.getPosition().getX(),
				rover.getPosition().getY(),
				rover.getCardinalDirection());
	}
}

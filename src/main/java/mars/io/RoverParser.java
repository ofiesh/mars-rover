package mars.io;

import mars.plateau.CardinalDirection;
import mars.plateau.Plateau;
import mars.plateau.Position;
import mars.rover.Rover;

public class RoverParser {
	private final Plateau plateau;

	public RoverParser(Plateau plateau) {
		this.plateau = plateau;
	}

	public Rover parseRover(String input) throws RoverInputException {
		try {
			String[] inputs = input.split(" ");
			if (inputs.length != 3) {
				throw new RoverInputException("Rover input must be in the form of '(Number Number Direction')");
			}

			Integer x = Integer.parseInt(inputs[0]);
			Integer y = Integer.parseInt(inputs[1]);

			if(plateau.getLength() < y || y < 0) {
				throw new RoverInputException("Rover start y is not on the plateau");
			}
			if(plateau.getWidth() < x || x < 0) {
				throw new RoverInputException("Rover start x is not on the plateau");
			}

			Rover rover = new Rover();
			rover.setPosition(new Position(Integer.parseInt(inputs[0]), Integer.parseInt(inputs[1])));
			rover.setCardinalDirection(CardinalDirection.valueOf(inputs[2]));
			return rover;
		} catch(NumberFormatException e) {
			throw new RoverInputException("Rover start coordinates must be valid integers");
		} catch(IllegalArgumentException e) {
			throw new RoverInputException("Direction must be N S E or W");
		}
	}
}

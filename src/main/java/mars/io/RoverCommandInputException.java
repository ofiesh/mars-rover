package mars.io;

public class RoverCommandInputException extends InputException {
	public RoverCommandInputException(String message) {
		super(message);
	}
}

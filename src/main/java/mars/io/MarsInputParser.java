package mars.io;

import javafx.util.Pair;
import mars.mission.MarsMission;
import mars.plateau.Plateau;
import mars.rover.MoveCommand;
import mars.rover.Rover;

import java.util.ArrayList;
import java.util.List;

public class MarsInputParser {
	public MarsMission parseMarsMission(String[] input) throws InputException {
		PlateauParser plateauParser = new PlateauParser();
		Plateau plateau = plateauParser.parsePlateau(input[0]);

		RoverParser roverParser = new RoverParser(plateau);
		RoverCommandParser roverCommandParser = new RoverCommandParser(plateau);
		List<Pair<Rover, List<MoveCommand>>> roverCommandPairs = new ArrayList<>();
		for(int i = 1; i < input.length; i += 2) {
			Rover rover = roverParser.parseRover(input[i]);
			List<MoveCommand> moveCommands = roverCommandParser.parseMoveCommands(input[i + 1]);
			roverCommandPairs.add(new Pair<>(rover, moveCommands));
		}
		return new MarsMission(plateau, roverCommandPairs);
	}
}

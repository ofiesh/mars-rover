package mars.io;

import mars.plateau.Plateau;
import mars.rover.MoveCommand;
import mars.rover.PlateauMoveForwardCommand;
import mars.rover.TurnCommand;
import mars.plateau.TurnDirection;

import java.util.ArrayList;
import java.util.List;

public class RoverCommandParser {
	private final Plateau plateau;

	public RoverCommandParser(Plateau plateau) {
		this.plateau = plateau;
	}

	public List<MoveCommand> parseMoveCommands(String input) throws RoverCommandInputException {
		ArrayList<MoveCommand> commands = new ArrayList<>();

		for(String command : input.split("")) {
			switch (command) {
				case "L":
				case "R":
					commands.add(new TurnCommand(TurnDirection.valueOf(command)));
					break;
				case "M":
					commands.add(new PlateauMoveForwardCommand(plateau));
					break;
				default:
					throw new RoverCommandInputException("Rover command must be L, R or M");
			}
		}

		return commands;
	}
}

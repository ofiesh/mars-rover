package mars.io;

public class RoverInputException extends InputException {

	public RoverInputException(String message) {
		super(message);
	}
}

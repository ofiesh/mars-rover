package mars.io;

public class PlateauInputException extends InputException {

	public PlateauInputException(String message) {
		super(message);
	}
}

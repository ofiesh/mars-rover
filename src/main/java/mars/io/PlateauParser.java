package mars.io;

import mars.plateau.Plateau;

public class PlateauParser {
	public Plateau parsePlateau(String input) throws PlateauInputException {
		try {
			String[] plateauInput = input.split(" ");
			if (plateauInput.length != 2) {
				throw new PlateauInputException("Plateau input must be of form 'Number Number'");
			}
			Integer x = Integer.parseInt(plateauInput[0]);
			Integer y = Integer.parseInt(plateauInput[1]);
			if(x < 0) {
				throw new PlateauInputException("Plateau x must be greater than 0");
			}
			if(y < 0) {
				throw new PlateauInputException("Plateau y must be greater than 0");
			}
			return new Plateau(x, y);
		} catch (NumberFormatException e) {
			throw new PlateauInputException("Plateau coordinates must be valid Integers");
		}
	}
}

package mars.io;

public class InputException extends Exception {
	private final String message;

	public InputException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}

package mars.rover;

import mars.plateau.Plateau;
import mars.plateau.Position;

public class PlateauMoveForwardCommand implements MoveCommand {
	private final Plateau plateau;

	public PlateauMoveForwardCommand(Plateau plateau) {
		this.plateau = plateau;
	}

	public void move(Rover rover) throws RoverFellOffPlateauException {
		Position position = rover.getCardinalDirection().move(rover.getPosition());
		if(position.getX() > plateau.getWidth() || position.getY() > plateau.getLength()
				|| position.getX() < 0 || position.getY() < 0) {
			throw new RoverFellOffPlateauException();
		}
		rover.setPosition(rover.getCardinalDirection().move(rover.getPosition()));
	}
}

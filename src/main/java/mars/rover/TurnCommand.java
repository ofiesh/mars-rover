package mars.rover;

import mars.plateau.TurnDirection;

public class TurnCommand implements MoveCommand {
	public static final TurnCommand L = new TurnCommand(TurnDirection.L);
	public static final TurnCommand R = new TurnCommand(TurnDirection.R);
	private final TurnDirection turnDirection;

	public TurnCommand(TurnDirection turnDirection) {
		this.turnDirection = turnDirection;
	}

	public void move(Rover rover) {
		rover.setCardinalDirection(rover.getCardinalDirection().rotate(turnDirection));
	}

	public TurnDirection getTurnDirection() {
		return turnDirection;
	}
}

package mars.rover;

public interface MoveCommand {
	void move(Rover rover) throws MoveCommandException;
}

package mars.rover;

import mars.plateau.CardinalDirection;
import mars.plateau.Position;

public class Rover {
	private Position position;
	private CardinalDirection cardinalDirection;

	public Position getPosition() {
		return position;
	}

	public CardinalDirection getCardinalDirection() {
		return cardinalDirection;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public void setCardinalDirection(CardinalDirection cardinalDirection) {
		this.cardinalDirection = cardinalDirection;
	}
}

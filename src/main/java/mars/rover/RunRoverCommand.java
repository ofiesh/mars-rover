package mars.rover;

import java.util.List;

public class RunRoverCommand {
	public void runCommand(Rover rover, List<MoveCommand> commands) throws MoveCommandException {
		for(MoveCommand command : commands) {
			command.move(rover);
		}
	}
}

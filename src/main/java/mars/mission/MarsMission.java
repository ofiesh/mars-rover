package mars.mission;

import javafx.util.Pair;
import mars.plateau.Plateau;
import mars.rover.MoveCommand;
import mars.rover.Rover;

import java.util.List;

public class MarsMission {
	private final Plateau plateau;
	private final List<Pair<Rover, List<MoveCommand>>> roverCommandPairs;

	public MarsMission(Plateau plateau, List<Pair<Rover, List<MoveCommand>>> roverCommandPairs) {
		this.plateau = plateau;
		this.roverCommandPairs = roverCommandPairs;
	}

	public Plateau getPlateau() {
		return plateau;
	}

	public List<Pair<Rover, List<MoveCommand>>> getRoverCommandPairs() {
		return roverCommandPairs;
	}
}

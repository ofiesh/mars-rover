package mars.mission;

import javafx.util.Pair;
import mars.io.InputException;
import mars.io.MarsInputParser;
import mars.io.RoverOutputFormatter;
import mars.rover.MoveCommand;
import mars.rover.MoveCommandException;
import mars.rover.Rover;
import mars.rover.RunRoverCommand;

import java.util.ArrayList;
import java.util.List;

public class MarsMissionControl {
	public List<String> runMission(String[] missionInput) throws MoveCommandException, InputException {
		RoverOutputFormatter roverOutputFormatter = new RoverOutputFormatter();
		RunRoverCommand runRover = new RunRoverCommand();
		MarsMission marsMission = new MarsInputParser().parseMarsMission(missionInput);

		List<String> output = new ArrayList<>();
		for(Pair<Rover, List<MoveCommand>> roverCommandPair : marsMission.getRoverCommandPairs()) {
			runRover.runCommand(roverCommandPair.getKey(), roverCommandPair.getValue());
			output.add(roverOutputFormatter.format(roverCommandPair.getKey()));
		}
		return output;
	}
}

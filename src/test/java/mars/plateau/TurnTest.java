package mars.plateau;

import org.junit.Assert;
import org.junit.Test;


public class TurnTest {
	@Test
	public void testNTurnL() {
		Assert.assertEquals(CardinalDirection.W, CardinalDirection.N.rotate(TurnDirection.L));
	}

	@Test
	public void testNTurnR() {
		Assert.assertEquals(CardinalDirection.E, CardinalDirection.N.rotate(TurnDirection.R));
	}

	@Test
	public void testSTurnL() {
		Assert.assertEquals(CardinalDirection.E, CardinalDirection.S.rotate(TurnDirection.L));
	}

	@Test
	public void testSTurnR() {
		Assert.assertEquals(CardinalDirection.W, CardinalDirection.S.rotate(TurnDirection.R));
	}

	@Test
	public void TestETurnL() {
		Assert.assertEquals(CardinalDirection.N, CardinalDirection.E.rotate(TurnDirection.L));
	}

	@Test
	public void testETurnR() {
		Assert.assertEquals(CardinalDirection.S, CardinalDirection.E.rotate(TurnDirection.R));
	}

	@Test
	public void testWTurnL() {
		Assert.assertEquals(CardinalDirection.S, CardinalDirection.W.rotate(TurnDirection.L));
	}

	@Test
	public void testWTurnR() {
		Assert.assertEquals(CardinalDirection.N, CardinalDirection.W.rotate(TurnDirection.R));
	}
}

package mars.plateau;

import org.junit.Assert;
import org.junit.Test;

public class MoveTest {
	private final Position position = new Position(2, 3);
	@Test
	public void testMoveN() {
		Position position = CardinalDirection.N.move(this.position);
		Assert.assertEquals(new Position(2, 4), position);
	}

	@Test
	public void testMoveW() {
		Position position = CardinalDirection.W.move(this.position);
		Assert.assertEquals(new Position(1, 3), position);
	}

	@Test
	public void testMoveS() {
		Position position = CardinalDirection.S.move(this.position);
		Assert.assertEquals(new Position(2, 2), position);
	}

	@Test
	public void testMoveE() {
		Position position = CardinalDirection.E.move(this.position);
		Assert.assertEquals(new Position(3, 3), position);
	}
}

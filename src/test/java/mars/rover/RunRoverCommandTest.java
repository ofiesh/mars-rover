package mars.rover;

import mars.plateau.CardinalDirection;
import mars.plateau.Plateau;
import mars.plateau.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class RunRoverCommandTest {
	private final Plateau plateau = new Plateau(4, 3);
	private final RunRoverCommand runRoverCommand = new RunRoverCommand();
	private final List<MoveCommand> commands = Arrays.asList(
			new PlateauMoveForwardCommand(plateau),
			TurnCommand.L,
			new PlateauMoveForwardCommand(plateau));
	private Rover rover;

	@Before
	public void setup() {
		rover = new Rover();
		rover.setCardinalDirection(CardinalDirection.S);
		rover.setPosition(new Position(3, 3));
	}

	@Test
	public void testRunCommandPosition() throws MoveCommandException {
		runRoverCommand.runCommand(rover, commands);
		Assert.assertEquals(new Position(4, 2), rover.getPosition());
	}

	@Test
	public void testRunCommandDirection() throws MoveCommandException {
		runRoverCommand.runCommand(rover, commands);
		Assert.assertEquals(CardinalDirection.E, rover.getCardinalDirection());
	}

	@Test(expected = RoverFellOffPlateauException.class)
	public void testFallOffX() throws MoveCommandException {
		runRoverCommand.runCommand(rover, Arrays.asList(
				TurnCommand.L, new PlateauMoveForwardCommand(plateau), new PlateauMoveForwardCommand(plateau)));
	}

	@Test(expected = RoverFellOffPlateauException.class)
	public void testFallOffY() throws MoveCommandException {
		runRoverCommand.runCommand(rover, Arrays.asList(
				new PlateauMoveForwardCommand(plateau),
				new PlateauMoveForwardCommand(plateau),
				new PlateauMoveForwardCommand(plateau),
				new PlateauMoveForwardCommand(plateau)
		));
		System.out.println(rover.getPosition());
	}
}

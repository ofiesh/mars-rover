package mars.io;

import mars.plateau.CardinalDirection;
import mars.plateau.Plateau;
import org.junit.Assert;
import org.junit.Test;

public class RoverParserTest {
	private final RoverParser parser = new RoverParser(new Plateau(3, 4));

	@Test
	public void testX() throws RoverInputException {
		Assert.assertEquals(new Integer(3), parser.parseRover("3 4 E").getPosition().getX());
	}

	@Test
	public void testY() throws RoverInputException {
		Assert.assertEquals(new Integer(4), parser.parseRover("3 4 E").getPosition().getY());
	}

	@Test
	public void testDirection() throws RoverInputException {
		Assert.assertEquals(CardinalDirection.E, parser.parseRover("3 4 E").getCardinalDirection());
	}

	@Test(expected = RoverInputException.class)
	public void testInvalidX() throws RoverInputException {
		parser.parseRover("4 4 E");
	}

	@Test(expected = RoverInputException.class)
	public void testNegX() throws RoverInputException {
		parser.parseRover("-1 4 E");
	}

	@Test(expected = RoverInputException.class)
	public void testInvalidY() throws RoverInputException {
		parser.parseRover("3 5 E");
	}

	@Test(expected = RoverInputException.class)
	public void testNegY() throws RoverInputException {
		parser.parseRover("3 -1 E");
	}

	@Test(expected = RoverInputException.class)
	public void testLengthShort() throws RoverInputException {
		parser.parseRover("3 1");
	}

	@Test(expected = RoverInputException.class)
	public void testLengthLong() throws RoverInputException {
		parser.parseRover("3 1 N N");
	}

	@Test(expected = RoverInputException.class)
	public void testInvalidInteger() throws RoverInputException {
		parser.parseRover("3 N N");
	}

	@Test(expected = RoverInputException.class)
	public void testInvalidDirection() throws RoverInputException {
		parser.parseRover("3 4 NE");
	}
}

package mars.io;

import mars.plateau.CardinalDirection;
import mars.plateau.Position;
import mars.rover.Rover;
import org.junit.Assert;
import org.junit.Test;

public class OutputTest {
	@Test
	public void testRoverOutput() {
		Rover rover = new Rover();
		rover.setPosition(new Position(3, 1));
		rover.setCardinalDirection(CardinalDirection.S);
		Assert.assertEquals("3 1 S", new RoverOutputFormatter().format(rover));
	}
}

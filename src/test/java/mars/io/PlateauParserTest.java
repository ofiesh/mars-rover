package mars.io;

import org.junit.Assert;
import org.junit.Test;

public class PlateauParserTest {
	PlateauParser parser = new PlateauParser();
	@Test
	public void testX() throws PlateauInputException {
		Assert.assertEquals(new Integer(3), parser.parsePlateau("3 5").getWidth());
	}

	@Test
	public void testY() throws PlateauInputException {
		Assert.assertEquals(new Integer(5), parser.parsePlateau("3 5").getLength());
	}

	@Test(expected = PlateauInputException.class)
	public void testNegX() throws PlateauInputException {
		parser.parsePlateau("-1 5");
	}

	@Test(expected = PlateauInputException.class)
	public void testNegY() throws PlateauInputException {
		parser.parsePlateau("3 -1");
	}

	@Test(expected = PlateauInputException.class)
	public void testInvalidInteger() throws PlateauInputException {
		parser.parsePlateau("S 3");
	}

	@Test(expected = PlateauInputException.class)
	public void testLengthLong() throws PlateauInputException {
		parser.parsePlateau("4 1 2");
	}

	@Test(expected = PlateauInputException.class)
	public void testLengthShort() throws PlateauInputException {
		parser.parsePlateau("1");

	}
}

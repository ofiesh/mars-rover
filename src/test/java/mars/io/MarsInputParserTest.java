package mars.io;

import org.junit.Test;

public class MarsInputParserTest {
	@Test
	public void testValidInput() throws InputException {
		String[] input = {"5 5", "3 5 N", "RMLM", "5 1 E", "MMM"};
		new MarsInputParser().parseMarsMission(input);
	}

	@Test(expected = RoverInputException.class)
	public void testMixedRoverCommand() throws InputException {
		String[] input = {"5 5", "RMLM", "3 2 N"};
		new MarsInputParser().parseMarsMission(input);
	}

	@Test(expected = RoverInputException.class)
	public void testMixed2ndRoverCommand() throws InputException {
		String[] input = {"5 5", "5 1 E", "MMM", "RMLM", "3 2 N"};
		new MarsInputParser().parseMarsMission(input);
	}
}

package mars.io;

import mars.rover.PlateauMoveForwardCommand;
import mars.rover.TurnCommand;
import mars.plateau.TurnDirection;
import org.junit.Assert;
import org.junit.Test;

public class RoverCommandParserTest {
	private final RoverCommandParser parser = new RoverCommandParser(null);
	@Test(expected = RoverCommandInputException.class)
	public void testInvalidRoverCommand() throws RoverCommandInputException {
		parser.parseMoveCommands("LRMS");
	}

	@Test
	public void testTurnLeftCommand() throws RoverCommandInputException {
		Assert.assertEquals(TurnDirection.L, ((TurnCommand) parser.parseMoveCommands("LMR").get(0)).getTurnDirection());
	}

	@Test
	public void testTurnRightCommand() throws RoverCommandInputException {
		Assert.assertEquals(TurnDirection.R, ((TurnCommand) parser.parseMoveCommands("LMR").get(2)).getTurnDirection());
	}

	@Test
	public void testMoveCommand() throws RoverCommandInputException {
		Assert.assertTrue(parser.parseMoveCommands("LMR").get(1) instanceof PlateauMoveForwardCommand);
	}
}

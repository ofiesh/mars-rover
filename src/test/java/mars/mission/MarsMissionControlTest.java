package mars.mission;

import mars.io.InputException;
import mars.rover.MoveCommandException;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MarsMissionControlTest {
	String[] input = {
			"5 5",
			"1 2 N",
			"LMLMLMLMM",
			"3 3 E",
			"MMRMMRMRRM"
	};

	@Test
	public void testMission() throws MoveCommandException, InputException {
		List<String> output = new MarsMissionControl().runMission(input);

		System.out.println(output);
		Assert.assertThat(output, CoreMatchers.is(Arrays.asList(
				"1 3 N",
				"5 1 E"
		)));
	}
}
